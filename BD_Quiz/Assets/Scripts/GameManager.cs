﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    private bool _highscoresFlag;
    private int _score;
    private int _category;
    private int _difficulty;
    private int _playerID;
    private string _playerName;
    private Question _actualQuestion;
    private Answer _chosenAnswer;
    private List<Question> _questionsList;
    private List<Answer> _answersList;
    private Canvas _canvas;
    private AudioSource _audio;

    [Header("Database Manager")]
    [SerializeField] private DatabaseManager _dbManager;
    [Header("Game Elements")]
    [SerializeField] private GameObject _scoreView;
    [SerializeField] private GameObject _timer;
    [SerializeField] private GameObject _question;
    [SerializeField] private GameObject _answer1;
    [SerializeField] private GameObject _answer2;
    [SerializeField] private GameObject _answer3;
    [SerializeField] private GameObject _answer4;

    private void Awake()
    {
        _highscoresFlag = false;
        _canvas = FindObjectOfType<Canvas>();
        _audio = GetComponent<AudioSource>();
        _difficulty = 1;
        _score = 0;
        UpdateScore();
    }

    private void ShowHighscores()
    {
        TextMeshProUGUI hscoresScreen = 
            _canvas.transform.GetChild(2).GetChild(4).
            GetComponentInChildren<TextMeshProUGUI>();

        List<string> highscores = DatabaseManager.Instance.RetrieveHighscores();

        if (highscores != null)
        {
            hscoresScreen.text = "";
            foreach (string s in highscores)
            {
                hscoresScreen.text += s + "\n";
            }
        }
    }

    public void NextMenu(int index)
    {
        if (!_highscoresFlag)
        {
            _highscoresFlag = true;
            ShowHighscores();
        }

        GameObject currentMenu =
            _canvas.transform.GetChild(index).gameObject;
        GameObject nextMenu =
            _canvas.transform.GetChild(++index).gameObject;

        currentMenu.SetActive(false);
        nextMenu.SetActive(true);
    }

    private void PreviousMenu(int index)
    {
        GameObject currentMenu =
            _canvas.transform.GetChild(index).gameObject;
        GameObject previousMenu =
            _canvas.transform.GetChild(--index).gameObject;

        currentMenu.SetActive(false);
        previousMenu.SetActive(true);
    }

    public void CreatePlayer()
    {
        _playerName =_canvas.transform.GetChild(2).
            GetChild(2).GetComponent<TMP_InputField>().text;

        Debug.Log(_playerName);

        DatabaseManager.Instance.CreatePlayer(_playerName);
        _playerID = DatabaseManager.Instance.GetLastPlayerID();

        Debug.Log(_playerID);
        NextMenu(2);
    }

    public void SetGame(int category)
    {
        // SetGame is executed each time new questions/answers are shown,
        // And an answer button is always highlighted after the first click.
        // This way we disable the selection
        EventSystem.current.SetSelectedGameObject(null);

        _category = category;
        int questionIndex;

        // Retrieve questions of difficulty X to list and choose a random one
        _questionsList = 
            DatabaseManager.Instance.RetrieveQuestions(category, _difficulty);

        questionIndex = UnityEngine.Random.Range(0, _questionsList.Count);
        _actualQuestion = _questionsList[questionIndex];

        // Retrieve answers of the specific question
        _answersList = 
            DatabaseManager.Instance.RetrieveAnswers(_actualQuestion.QuestionID);

        NextMenu(3);
        ShowQA();
        StartCoroutine(Countdown(15));
    }

    private void ShowQA()
    {
        _question.GetComponentInChildren<TextMeshProUGUI>().text = 
            _actualQuestion.QuestionText;
        _answer1.GetComponentInChildren<TextMeshProUGUI>().text = 
            _answersList[0].AnswerText;
        _answer2.GetComponentInChildren<TextMeshProUGUI>().text = 
            _answersList[1].AnswerText;
        _answer3.GetComponentInChildren<TextMeshProUGUI>().text = 
            _answersList[2].AnswerText;
        _answer4.GetComponentInChildren<TextMeshProUGUI>().text = 
            _answersList[3].AnswerText;
    }

    public void ChooseAnswer(int answer)
    {
        StopAllCoroutines();

        if (_answersList[answer].IsCorrect)
        {
            ManageScore();
            DatabaseManager.Instance.SetPlayerScore(_playerID, _score);
            RefreshGame();
        }
        else
        {
            _audio.Stop();
            GameOver();
        }
    }

    public void ManageScore()
    {
        _score++;
        UpdateScore();
    }

    private void UpdateScore()
    {
        _scoreView.GetComponentInChildren<TextMeshProUGUI>().text = 
            $"Score: {_score}";
    }

    private void RefreshGame()
    {
        if (_difficulty == 3)
        {
            _audio.Stop();
            _difficulty = 1;
            PreviousMenu(4);
        }
        else
        {
            _difficulty++;
            SetGame(_category);
        }
    }

    private void GameOver()
    {
        NextMenu(4);
        UpdateGameOverScore();
    }

    private void UpdateGameOverScore()
    {
        _canvas.transform.GetChild(5).GetChild(2).GetComponent<TMP_InputField>()
            .placeholder.GetComponent<TextMeshProUGUI>().text = $"SCORE: {_score}";
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private IEnumerator Countdown(int time)
    {
        TextMeshProUGUI timerTxt = 
            _timer.GetComponentInChildren<TextMeshProUGUI>();

        timerTxt.text = time.ToString();
        _audio.Play();

        while (time > 0)
        {
            time--;
            yield return new WaitForSeconds(1);
            timerTxt.text = time.ToString();
        }

        GameOver();
    }
}

public struct Answer
{
    public int AnswerID { get; set; }
    public string AnswerText { get; set; }
    public int RespectiveQuestionID { get; set; }
    public bool IsCorrect { get; set; }
}

public struct Question
{
    public int QuestionID { get; set; }
    public string QuestionText { get; set; }
    public int RespectiveCategoryID { get; set; }
    public int Difficulty { get; set; }
}
