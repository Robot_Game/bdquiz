﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using UnityEngine;

public class DatabaseManager : MonoBehaviour
{
    private string _tableAnswers = "Answers";
    private string _tableQuestions = "Questions";
    private string _tablePlayers = "Players";

    private SqlConnection _connection;
    private static DatabaseManager _instance;

    public DatabaseConnectionParams ConnectionParams { get; set; }

    public static DatabaseManager Instance => _instance;

    public void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
    }

    public void Connect()
    {
        if (_connection != null) return;

        string connectionStr = "";

        connectionStr += $"Data Source={ConnectionParams.dataSource};";
        connectionStr += $"Initial Catalog={ConnectionParams.databaseName};";
        connectionStr += $"User ID={ConnectionParams.username};";
        connectionStr += $"Password={ConnectionParams.password};";

        _connection = new SqlConnection(connectionStr);
        try
        {
            Debug.Log("Connecting...");
            _connection.Open();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to connect: " + e.Message);
        }

        if (_connection != null) Debug.Log("Connected!");
    }

    public List<string> RetrieveHighscores()
    {
        List<string> highscores = new List<string>();
        SqlDataReader dataReader = null;

        RunQuery($"SELECT TOP (10) * FROM {_tablePlayers} " +
            $"ORDER BY score DESC;"
            , ref dataReader);
        {
            while (dataReader.Read())
            {
                highscores.Add($"" +
                    $"{dataReader.GetString(dataReader.GetOrdinal("name"))}" + 
                    " - " + 
                    $"{dataReader.GetInt32(dataReader.GetOrdinal("score"))}");
            }
        }

        if (dataReader != null) dataReader.Close();

        return highscores;
    }

    public void CreatePlayer(string name)
    {
        string createPlayerQuery = "";

        createPlayerQuery += $"INSERT INTO {_tablePlayers} VALUES (";
        createPlayerQuery += $"'{name}', ";
        createPlayerQuery += "0";
        createPlayerQuery += ");";

        RunQuery(createPlayerQuery);
    }

    public int GetLastPlayerID()
    {
        int id = 0;
        SqlDataReader dataReader = null;

        RunQuery($"SELECT MAX(ID) AS LastID FROM {_tablePlayers};", ref dataReader);
        {
            while (dataReader.Read())
            {
                id = dataReader.GetInt32(dataReader.GetOrdinal("LastID"));
                break;
            }
        }

        if (dataReader != null) dataReader.Close();

        return id;
    }

    public void SetPlayerScore(int id, int score)
    {
        string setPlayerScoreQuery = "";

        setPlayerScoreQuery += $"UPDATE {_tablePlayers} ";
        setPlayerScoreQuery += $"SET score = {score} ";
        setPlayerScoreQuery += $"WHERE (ID = {id}";
        setPlayerScoreQuery += ");";

        RunQuery(setPlayerScoreQuery);
    }

    public List<Question> RetrieveQuestions(int category, int dif)
    {
        List<Question> questionsList = new List<Question>();
        SqlDataReader dataReader = null;

        RunQuery($"SELECT * FROM {_tableQuestions} " +
            $"WHERE (categoryID = {category} AND difficulty = {dif});"
            , ref dataReader);
        {
            while (dataReader.Read())
            {
                questionsList.Add(new Question()
                {
                    QuestionID = 
                        dataReader.GetInt32(dataReader.GetOrdinal("questionID")),
                    QuestionText = 
                        dataReader.GetString(dataReader.GetOrdinal("question")),
                    RespectiveCategoryID = 
                        dataReader.GetInt32(dataReader.GetOrdinal("categoryID")),
                    Difficulty = 
                        dataReader.GetInt32(dataReader.GetOrdinal("difficulty"))
                });
            }
        }

        if (dataReader != null) dataReader.Close();

        return questionsList;
    }

    public List<Answer> RetrieveAnswers(int questionID)
    {
        List<Answer> answersList = new List<Answer>();
        SqlDataReader dataReader = null;

        RunQuery($"SELECT * FROM {_tableAnswers} " +
            $"WHERE (questionID = {questionID});"
            , ref dataReader);
        {
            while (dataReader.Read())
            {
                answersList.Add(new Answer()
                {
                    AnswerID = 
                        dataReader.GetInt32(dataReader.GetOrdinal("answerID")),
                    AnswerText = 
                        dataReader.GetString(dataReader.GetOrdinal("answer")),
                    RespectiveQuestionID = 
                        dataReader.GetInt32(dataReader.GetOrdinal("questionID")),
                    IsCorrect = 
                        (dataReader.GetInt32(dataReader.GetOrdinal(
                            "isCorrectAnswer")) == 0) ? (false) : (true)
                });
            }
        }

        if (dataReader != null) dataReader.Close();

        return answersList;
    }

    private void RunQuery(string query)
    {
        SqlCommand sql = new SqlCommand(query, _connection);
        try
        {
            sql.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
        }

        Debug.Log(query);
    }

    private void RunQuery(string query, ref SqlDataReader reader)
    {
        SqlCommand sql = new SqlCommand(query, _connection);
        try
        {
            reader = sql.ExecuteReader();
        }
        catch (Exception e)
        {
            Debug.LogError("Failed to run query: " + query);
            Debug.LogError("Error: " + e.Message);
        }

        Debug.Log(query);
    }
}
