﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseConnectionManager : MonoBehaviour
{
    [SerializeField] private DatabaseConnectionParams _localConnection;
    [SerializeField] private DatabaseConnectionParams _remoteConnection;

    public void SelectLocalConnection()
    {
        DatabaseManager.Instance.ConnectionParams = _localConnection;
        DatabaseManager.Instance.Connect();
    }

    public void SelectRemoteConnection()
    {
        DatabaseManager.Instance.ConnectionParams = _remoteConnection;
        DatabaseManager.Instance.Connect();
    }
}
